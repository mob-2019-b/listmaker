package com.example.listmaker


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_list_detail.*

/**
 * A simple [Fragment] subclass.
 */
class ListDetailFragment : Fragment() {

    lateinit var list: TaskList
    lateinit var itemsRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            list = it.getParcelable(MainActivity.INTENT_LIST_KEY)!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list_detail, container, false)

        view?.let {
            itemsRecyclerView = it.findViewById(R.id.list_item_recyclerview)
            itemsRecyclerView.adapter = ListItemsAdapter(list.tasks)
            itemsRecyclerView.layoutManager = LinearLayoutManager(context)
        }

        return view
    }

    fun addTask(item: String) {
        list.tasks.add(item)

        val adapter = itemsRecyclerView.adapter as ListItemsAdapter
        adapter.notifyItemInserted(list.tasks.size - 1)
    }

    companion object {

        private const val ARG_LIST = "list"

        fun newInstance(list: TaskList): ListDetailFragment {
            val fragment = ListDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_LIST, list)
            fragment.arguments = args
            return fragment
        }
    }


}
